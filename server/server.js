const express = require('express');
const app = express();
require('./config/config');
const mongoose = require('mongoose');
const rutas = require('./routes/index');
const bodyParser = require('body-parser');

// bodyParser
app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json());

app.use('/api/lucky', rutas);

mongoose.connect(process.env.URLDB, { useNewUrlParser: true, useCreateIndex: true }, (err,res) => {
  if(err) throw err;

  console.log('BBDD Online');
});
//console.log(process.env);
//console.log("hoy es "+ new Date());
app.listen(process.env.PORT, () => {
  console.log('Escuchando peticiones en el puerto: ', 3000);
});