const express = require('express');
const app = express();

const novedadModel = require('../models/novedad');

app.get('/novedad', (req, res) => {
    novedadModel.find({}, (err, novedades) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'novedades get',
            novedades
        });
    });
});

app.get('/novedad/:id', (req, res) => {
    let id = req.params;

    novedadModel.find(id, (err, novedad) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'novedades get',
            novedad
        });
    });
});

app.post('/novedad', (req, res) => {

    let body = req.body;

    let novedad = new novedadModel({
        titulo: body.titulo,
        descripcion: body.descripcion
    });

    novedad.save((err, novedadDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'novedad insertada correctamente en BBDD',
            novedad: novedadDB
        });
    });
});

module.exports = app;
