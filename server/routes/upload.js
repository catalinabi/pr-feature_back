const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const asociacionModel = require('../models/asociacion')
const usuarioModel = require('../models/usuario')
const novedadModel = require('../models/novedad')

const fs = require('fs');
const path = require('path');

app.use(fileUpload(
   // {createParentPath: true}
));

app.put('/upload/:tipo/:id', function(req, res){

    let tipo = req.params.tipo;
    let id = req.params.id;
    
    if(!req.files){
        return res.status(400).json({
            ok:false,
            err: {
                message: 'No se ha seleccionado ningún archivo'
            }
        })
    }

    // Validar tipo
    let tiposValidos = ['novedades','usuarios','asociaciones']
    if(tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({
            of: false,
            err: {
                message: 'Los tipos permitidos son: ' + tiposValidos.join()
            }
        })
    }

    let archivo = req.files.archivo;
    let nombreArchivoRecortado = archivo.name.split('.');
    let extension = nombreArchivoRecortado[nombreArchivoRecortado.length - 1];

    // Extensiones permitidas
    let extensionesPermitidas = ['png','jpg','gif','jpeg'];

    if(extensionesPermitidas.indexOf(extension) < 0) {
        return res.status(500).json({
            ok:false,
            err: {
                message: 'Las extensiones permitidas son ' + extensionesPermitidas.join(),
                ext: extension
            }
        })
    }

    // Cambiar nombre al archivo
    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`

    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
        if(err) {
            return res.status(500).json({
                ok:false,
                err
            })
        }

        //Aquí, imagen cargada
        if(tipo == 'usuarios'){
            imagenUsuario(id, res, nombreArchivo)    
        } else if(tipo == 'asociaciones'){
            imagenAsociacion(id, res, nombreArchivo)
        } else {
            imagenNovedad(id, res, nombreArchivo)
        }
        
    })
})

function imagenUsuario(id, res, nombreArchivo){

    usuarioModel.findById(id, (err, usuarioDB) => {
        if(err) {
            borrarArchivo(nombreArchivo, 'usuarios')
            return res.status(500).json({
                ok: false,
                mensaje: 'no existe el id ' + id,
                err
            })
        }
        //si el usuario no existe
        if (!usuarioDB) {
            borrarArchivo(nombreArchivo, 'usuarios')
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no existe'
                }
            })
        }

        borrarArchivo(usuarioDB.img, 'usuarios')

        usuarioDB.img = nombreArchivo;
        //guardo img en usuario
        usuarioDB.save((err, usuarioGuardado) => {
            res.json({
                ok: true,
                mensaje: 'imagen para idUsuario ->' + usuarioGuardado._id + ' insertada correctamente',
                usuario: usuarioGuardado,
                img: nombreArchivo
            })
        })
    })

}
//imagenAsociacion
function imagenAsociacion(id, res, nombreArchivo){

    asociacionModel.findById(id, (err, asociacionDB) => {
        if(err) {
            borrarArchivo(nombreArchivo, 'asociaciones')
            return res.status(500).json({
                ok: false,
                mensaje: 'no existe asociacion con id ' + id,
                err
            })
        }
        //si la Asociacion no existe
        if (!asociacionDB) {
            borrarArchivo(nombreArchivo, 'asociaciones')
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Asociacion no existe'
                }
            })
        }

        borrarArchivo(asociacionDB.img, 'asociaciones')

        asociacionDB.img = nombreArchivo;
        //guardo img en usuario
        asociacionDB.save((err, asociacionGuardada) => {
            res.json({
                ok: true,
                mensaje: 'imagen para idAsociacion ->' + asociacionGuardada._id + ' insertada correctamente',
                asociacion: asociacionGuardada,
                img: nombreArchivo
            })
        })
    })

}

function imagenNovedad(id, res, nombreArchivo){
    novedadModel.findById(id, (err, novedadDB) => {
        if(err) {
            borrarArchivo(nombreArchivo, 'novedades')
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!novedadDB) {
            borrarArchivo(nombreArchivo, 'novedades')
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Novedad no existe'
                }
            })
        }

        borrarArchivo(novedadDB.img, 'novedades')

        novedadDB.img = nombreArchivo;

        novedadDB.save((err, novedadGuardada) => {
            res.json({
                ok: true,
                novedad: novedadGuardada,
                img: nombreArchivo
            })
        })
    })
}
//function rescribir imagen
function borrarArchivo(nombreImagen, tipo) {
let pathImagen = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImagen}`);
    if( fs.existsSync(pathImagen)) {
        fs.unlinkSync(pathImagen);
    }
}

module.exports = app;
