const express = require("express");
const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");

const asociacion = require("../models/asociacion");
const app = express();
//LOGIN PAGE ASOCIACION

   app.post('/login-asociacion', (req, res) => {
    let asociacionEmail = req.body.email;
    let asociacionPass = req.body.password;

    //Si faltan o email o contraseña
    if (!asociacionEmail || !asociacionPass) {

        return res.status(400).json({
            ok: false,
            message: 'Debe introducir email y contraseña'
        });

    } else { //Si han metido email y password
        asociacion.findOne({ email: asociacionEmail }, (err, asociacionDB) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al obtener asociación',
                    err
                });
            }
            if (!asociacionDB) {
                return res.status(404).json({
                    ok: false,
                    message: 'Email de Asociacion o contraseña incorrectos'
                });
            }

            //Si la contraseña que introdujo la asociacion y la password de la db no son iguales
            if (!bcrypt.compare(asociacionPass, asociacionDB.password)) {
                //console.log(asociacionDB);
                //console.log("IMPRIMO" + asociacionPass + asociacionDB.password);
                return res.status(400).json({
                    ok: false,
                    message: 'Email de Asociacion o contraseña incorrectos'
                });

            }


            //generamos un token para la asociacion-> en 
            //middleware/autentication.js lo recogemos
            let token = jwt.sign({
                    // payload
                    asociacion: asociacionDB
                },
                process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN }
            );

            res.json({
                ok: true,
                asociacion: asociacionDB,
                token
            });

        });
    }



})


module.exports = app;