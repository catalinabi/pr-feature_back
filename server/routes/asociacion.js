const express = require('express');
const app = express();
const Asociacion = require('../models/asociacion');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

app.post('/asociacion', (req, res) => {
    
    
    let { nombre, direccion, email, password, telefono, nif } = req.body;
    console.log(`BODY recibimos ${nombre} ${direccion} ${email} ${password}  ${telefono} ${nif} `)

    if (!nombre || !direccion || !email || !password || !telefono || !nif) {

        return res.status(400).json({
            ok: false,
            message: 'Debe introducir todos los datos obligatorios'
        });

    } else { //Si han metido todos los datos obligatorios
        const asociacion = new Asociacion({
            nombre,
            direccion,
            email,
            password: bcrypt.hashSync(password, 10),
            telefono,
            nif

        });

        asociacion.save().then(result => {
            console.log(result);
            res.status(200).json({
                ok: true,
                message: "Asociacion creada con éxito",
                createdAsociacion: {
                    result,
                    request: {
                        type: 'GET',
                        url: "http://localhost:3000/asociacion/" + result._id,
                    },
                    img: {
                        url: "http://localhost:3000/" + result.img
                    }
                }
            });
        })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    ok: false,
                    message: "No se pudo crear la asociacion",
                    err
                });
            });
    }


})

app.get('/asociacion', (req, res, next) => {

    Asociacion.find({}).exec().then(docs => {
        const response = {
            count: docs.length,
            asociaciones: docs.map(asociacion => {
                return {
                    asociacion,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/asociacion/" + asociacion._id
                    },
                    img: {
                        url: "http://localhost:3000/" + asociacion.img
                    }
                };
            })
        };
        //Si hay asociaciones
        if (docs.length >= 0) {
            res.status(200).json({
                ok: true,
                message: "Listado de asociaciones:",
                response
            });
        } else {
            res.status(404).json({
                ok: false,
                message: 'No hemos encontrado asociaciones'
            });
        }
    })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                ok: false,
                message: 'Ha habido un error',
                error: err
            });
        });
})



module.exports = app;