const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const {verificarToken} = require('../middlewares/autenticacion');

const user = require('../models/usuario');

app.get('/usuario', verificarToken, function(req, res){

    user.find({}, (err,usuarios) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'usuarios get',
            usuarios
        });
    });
});

app.post('/usuario' , function(req, res) {

    console.log('usuarios post');

    let body = req.body;

    console.log(body)

    let usuario = new user({
        nombre: body.nombre,
        apellidos: body.apellidos,
        email: body.email,
        password: bcrypt.hashSync(body.password,10)
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            mensaje: 'usuario insertado correctamente en BBDD',
            usuario: usuarioDB
        });
    });

});

app.put('/usuario/:id', verificarToken, function(req, res){

    let id = req.params.id;
    let body = req.body;

    user.findByIdAndUpdate(id, body, {new: true}, (err, userDB) => {
        // Con el tercer parámetro, decimos que encuentre el objeto en BBDD y lo cambie por el cuerpo de la peticion
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(!userDB) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }
    
        res.json({
            ok: true,
            usuario : userDB
        });
    }); 
});

app.delete('/usuario/:id', verificarToken, function(req, res) {
    
    let id = req.params.id;

    user.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(!usuarioBorrado) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            usuario: usuarioBorrado
        });
    });
});

module.exports = app;