const express = require('express');
const app = express();
var fs = require('fs');
var path = require('path');
var Mascota = require('../models/mascota');
const {verificarToken} = require('../middlewares/autenticacionAsociacion');
const fecha = require('../libreria/fecha');


app.post('/mascota',verificarToken, (req, res) => {
   
        //metodo 
        let { nombre, especie, birthday, sexo, size, ciudad, peso, historia, personalidad, idAsociacion } = req.body;
        //console.log(`BODY recibimos ${nombre} ${especie} ${birthday} ${sexo} ${size} ${ciudad}  ${peso} ${historia} ${personalidad}`)
        let cumple = fecha.convertCumpleForDB(birthday);
        //Datos obligatorios
        if (!nombre || !especie || !birthday || !sexo || !size || !ciudad || !peso || !historia || !personalidad) {

            return res.status(400).json({
                ok: false,
                message: 'Debe introducir todos los datos obligatorios'
            });

        } else { //Si han metido todos los datos obligatorios

            const mascota = new Mascota({
                nombre,
                especie,
                birthday: cumple,
                sexo,
                size,
                ciudad,
                peso,
                historia,
                personalidad,
                idAsociacion: req.asociacion._id,

            });


            mascota.save().then(result => {
                    console.log(result);
                    res.status(200).json({
                        ok: true,
                        message: "Mascota creada con éxito",
                        createdMascota: {
                            mascota,
                            asociacionMascota: {
                                id: result.id
                            }
                        }
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        ok: false,
                        message: "No se pudo crear la mascota",
                        err
                    });
                });
        }

})
//Devuelve todas las mascotas
app.get('/mascota', (req, res, next) => {
    Mascota.find({ disponible: true }).exec().then(docs => {
        console.log(docs + docs.length);
        const response = {
            count: docs.length,
            mascotas: docs.map(mascota => {
                return {
                    mascota,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/mascotas/" + mascota._id
                    },
                    image: {
                        url: "http://localhost:3000/" + mascota.image
                    }
                };
            })
        };
        //Si hay mascotas 
        if (docs.length >= 0) {
            res.status(200).json({
                ok: true,
                message: "Listado de mascotas:",
                response
            });
        } else {
            res.status(404).json({
                ok: false,
                message: 'No hemos encontrado mascotas'
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            ok: false,
            message: 'Ha habido un error',
            error: err
        });
    });

    
})



module.exports = app;