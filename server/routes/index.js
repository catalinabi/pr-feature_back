const express = require('express');
const app = express();

app.use(require('./usuario'));
app.use(require('./login'));
app.use(require('./login-asociacion'));
app.use(require('./novedad'));
app.use(require('./upload'));
app.use(require('./imagenes'));
app.use(require('./asociacion'));
app.use(require('./mascota'));
module.exports = app;