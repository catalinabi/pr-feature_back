// ===================
// Puerto
// ===================

process.env.PORT = process.env.PORT || 3000;

// ===================
// Entorno
// ===================

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ===================
// Fecha expiracion token 
// ===================

process.env.CADUCIDAD_TOKEN = '48h';

// ===================
// SEED de autenticacion
// ===================

process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';

// ===================
// Configurar timeZone
// ===================
process.env.TZ = process.env.TZ || 'Europe/Amsterdam';

// ===================
// Base de datos
// ===================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    //urlDB = 'mongodb://localhost:27017/Lucky';
    urlDB = 'mongodb+srv://lucky:Lucky_2020@cluster0-veghe.mongodb.net/Lucky?retryWrites=true&w=majority';
} else {
    urlDB = 'mongodb+srv://lucky:Lucky_2020@cluster0-veghe.mongodb.net/Lucky?retryWrites=true&w=majority';
}

process.env.URLDB = urlDB;