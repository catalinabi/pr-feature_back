const mongoose = require('mongoose');

let schema = mongoose.Schema;

let novedadSchema = new schema({
    titulo: {
        type: String,
        required: [true, 'el titulo es obligatorio']
    },
    descripcion: {
        type: String,
        required: [true, 'la descripcion es obligatoria']
    },
    img: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model('novedad', novedadSchema);