let mongoose = require('mongoose');
let Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
const fecha = require('../libreria/fecha');
let hoy = fecha.getFechaHoy();
//validar Nif
const validateNif = nif => {
    var re = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;;
    return re.test(nif);
};
let ciudadesValidas = {
    values: ['madrid', 'barcelona'],
    message: '{VALUE} no es una ciudad válida'
};

let asociacionSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'el nombre de la asociacion es obligatorio'],
        unique: true,
    },
    direccion: {
        calle: String,
        ciudad: {
            type: String,
            required: [true, 'La ciudad es obligatoria'],
            enum: ciudadesValidas
        },
        cp: {
            type: Number,
            required: [true, 'el cp no es válido'],
            min: 28001,
            max: 28055
        }
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'el email es obligatorio']
    },
    password: {
        type: String,
        required: [true, 'la contraseña es obligatoria']
    },
    telefono: {
        type: Number,
        min: 100000000,
        max: 999999999,
        required: [true, 'el telefono es obligatorio']
    },

    img: {
        type: String,
        required: false
    },
    fechaRegistro: {
        type: Date,
        default: hoy
    },
    estado: {
        type: Boolean,
        default: true
    },
    nif: {
        type: String,
        require: [true, 'el NIF es necesario'],
        validate: [validateNif, 'El nif es invalido'],
        message: 'El nif es invalido'
    }
});

//Funcion que hace que no retornemos el password en formato JSON
asociacionSchema.methods.toJSON = function() {
    let asociacion = this;
    let asociacionObject = asociacion.toObject();
    delete asociacionObject.password;

    return asociacionObject;
}

//utilizamos el plugin uniquevalidator para añadir mensajes propios
asociacionSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' });
module.exports = mongoose.model('Asociacion', asociacionSchema);