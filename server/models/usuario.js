const mongoose = require('mongoose');

let schema = mongoose.Schema;

let usuarioSchema = new schema({
    nombre: {
        type: String,
        required: [true, 'el nombre es obligatorio']
    },
    apellidos: {
        type: String,
        required: [true, 'los apellidos son obligatorios']
    },
    email: {
        type: String,
        required: [true, 'el email es obligatorio']
    },
    password: {
        type: String,
        required: [true, 'la contraseña es obligatoria']
    },
    google: {
        type: Boolean,
        default: false
    },
    img: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model('user', usuarioSchema);