var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Prueba ahora-> para que me salga en la DB la hora local de Madrid
var ahora = new Date();
ahora.setMinutes(ahora.getMinutes() + 120);
console.log("hora formateada a hora Madrid "+ ahora);
//Prueba con libreria fecha
const fecha = require('../libreria/fecha');
let hoy = fecha.getFechaHoy();
//Cargo opciones de Schemaperro
const especiesValidas = {
    values: ['perro', 'gato', 'conejo', 'cobaya', 'pequeño mamifero', 'huron', 'pez', 'reptil', 'anfibio', 'insecto', 'ave'],
    message: '{VALUE} no es una especie válida'
};
const sexoValido = { values: ['macho', 'hembra'], message: 'El sexo {VALUE} no es válido' };
const sizeValido = { values: ['pequeño', 'mediano', 'grande'], message: 'El tamaño no es válido' };
const ciudadValida = { values: ['madrid', 'barcelona'], message: 'El campo ciudad {VALUE} no es válida' };
//Creando Schema Mascota
var mascotaSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'el nombre es obligatorio']
    },
    especie: {
        type: String,
        enum: especiesValidas,
        required: [true, 'especie obligatoria']
    },
    birthday: {
        type: Date,
        required: [true, 'fecha de cumpleaños obligatoria']
    },
    sexo: {
        type: String,
        enum: sexoValido,
        required: [true, 'sexo obligatorio']
    },
    size: {
        type: String,
        enum: sizeValido,
        required: [true, 'tamaño obligatorio']
    },
    ciudad: {
        type: String,
        enum: ciudadValida,
        required: [true, 'ciudad obligatoria']
    },
    peso: {
        type: String,
        required: [true, 'peso obligatorio']
    },
    historia: {
        type: String,
        required: [true, 'historia obligatoria']
    },
    personalidad: {
        type: Array,
        required: [true, 'personalidad obligatoria']
    },
    salud: {
        vacunado: {
            type: Boolean,
            default: true
        },
        desparasitado: {
            type: Boolean,
            default: true
        },
        sano: {
            type: Boolean,
            default: true
        },
        esterilizado: {
            type: Boolean,
            default: false,
        },
        identificado: {
            type: Boolean,
            default: true,
        },
        microchip: {
            type: Boolean,
            default: true,
        }
    },
    idAsociacion: { type: Schema.Types.ObjectId, ref: 'Asociacion', required: true },
    fechaEntradaAsociacion: {
        type: Date,
        default: hoy
    },
    numeroLikes: {
        type: Number
    },
    img: {
        type: String,
        required: false
    },
    disponible: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Mascota', mascotaSchema);