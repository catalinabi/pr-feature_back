const jwt = require("jsonwebtoken");

// ===============
// Verificar token
// ===============

let verificarToken = (req, res, next) => {

  let token = req.get("token");

  jwt.verify(token, process.env.NODE_ENV, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: "token no valido"
        }
      });
    }

    req.usuario = decoded.usuario;
    next();
  });
};

module.exports = {verificarToken};