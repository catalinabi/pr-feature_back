const jwt = require("jsonwebtoken");

// ===========================
// Verificar token asociacion
// ===========================

//next-> continua con la ejecucion del programa
let verificarToken = (req, res, next) => {

    //recojo en token el nombre del token que paso por headers con nombre token
    let token = req.get("token");

    //Funcion que verifica si el token enviado por headers es correcto
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                status: 'error',
                message: "token no valido"
            });
        }
        //Si consigues decodificar el token con info de asociacion
        //decoded payload es la asociacion...
        //si pegas en https://jwt.io/ el token te desglosa el decoded/payload y firma
        //guardo en req.asociacion la asociacion que se corresponde con el token enviado por headers
        req.asociacion = decoded.asociacion;

        //next-> hace que salga del middleware y ejecute la siguiente funcion 
        next();
    });
};


module.exports = {
    verificarToken
}